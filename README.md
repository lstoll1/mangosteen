# Mangosteen

Mangosteen is Crossref's member reports framework

A reporting framework to show members and the broader community information about our members and how well they participate in Crossref.

The reports can be extended via plugins so that members of the community can easily add their own analysis.

## FAQ

**Q:** Why "mangosteen?"

**A:** Mangosteens are always perfectly segmented (modular, if you will) and they don't look like much but are actually sweet and delicious. The symbolism is perfect.

## Installation

TODO

## Installation for development

- `git clone git@gitlab.com:crossref/labs/mangosteen.git`
- `cd mangosteen`
- `python -m venv venv && . ./venv/bin/activate`
- `pip install -e '.[dev]'`

### Refresh & prepare metadata

- `./refresh-member-data.sh`
- `python prepare-data.py`

## To create a new plugin

## To install a plugin

- copy plugins to either `plugins_available/crossref` or `plugins_available/community` as appropriate
- soft link plugins that you want to enable into `plugins_enabled`

## To run

- `streamlit run app.py`



## To bootstrap translation.

Make sure you download the appropriate `google-translate-service.json` credentials file from 1Password and put it someplace safe (not this repo!)

point `GOOGLE_APPLICATION_CREDENTIALS` to the above file:

```
export GOOGLE_APPLICATION_CREDENTIALS=<path_to_above_file>
```

Set the project ID

```
GOOGLE_CLOUD_PROJECT_ID=crossref-form-tranlator
```
(yes- it is misspelled- long, tedious story)

Run the shell script:

```
. ./bootstrap-translations.sh
```

More details on the script that does this here:

https://gitlab.com/crossref/tools/fluent-translate