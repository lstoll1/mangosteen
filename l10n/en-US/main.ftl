
my-first-string = English!
loading-data = Loading data...

 

splash-screen-heading = Splash Screen
results-screen-heading = Reports

member-selector-heading = Member selector
member-selector-label = Select member
member-selector-placeholder = Choose an option
member-selector-help = Select your favorite Crossref members

# The content filter allows the user to select subsections of content based
# on time period and content type.

content-filter-heading = Content filter

period-filter-label = Time period
period-filter-help = Select a publication time period
period-all-label = All
period-all-help = Both current and backfile content
period-current-label = Current
period-current-help = Content within the past two years
period-backfile-label = Backfile
period-back-file-help = Content two years or older

content-type-filter-label = Content type
content-type-filter-help = Select a content type

# Column labels and help

primary-name-label = Member Name
primary-name-help = The Crossref member's name
primary-resource-logo-label = Logo
primary-resource-logo-help = The member's logo as provided by [Clearbit](https://clearbit.com/) and based on the member's primary resource domain. 
id-label = Member ID
id-help = The Crossref member's REST API ID
account-type-label = Account Type
non-profit-label = Nonprofit
date-joined-label = Date Joined
annual-fee-label = Annual Fee
active-label = Active
board-status-label = Board Status
board-status-help = Is the member a current or past member of the Crossref board?
current-dois-label = Current DOIs
current-dois-help =  DOIs registered within the past three years
backfile-dois-label = Backfile DOIs
backfile-dois-help = DOIs registered three years ago or earlier
total-dois-label = Total DOIs
primary-resource-domain-label = Primary resource domain
primary-resource-domain-help = The domain that the member's DOIs most often point to
affiliations-current-label = Affiliations (current)
similarity-checking-current-label = Similarity Checking (current)
descriptions-current-label = Descriptions (current)
ror-ids-current-label = ROR IDs (current)
funders-backfile-label = Funders (backfile)
licenses-backfile-label = Licenses (backfile)
funders-current-label = Funders (current)
affiliations-backfile-label = Affiliations (current)
resource-links-backfile-label = Resource Links (backfile)
orcids-backfile-label = ORCID iDs (backfile)
update-policies-current-label = Update Policies (current)
ror-ids-backfile-label = ROR IDs (backfile)
orcids-current-label = ORCID iDs (current)
similarity-checking-backfile-label = Similarity Checking (backfile)
references-backfile-label = References (backfile)
descriptions-backfile-label = Descriptions (backfile)
award-numbers-backfile-label = Award Numbers (backfile)
update-policies-backfile-label = Update Policies (backfile)
licenses-current-label = Licenses (current)
award-numbers-current-label = Award Numbers (current)
abstracts-backfile-label = Abstracts (backfile)
resource-links-current-label = Resource Links (current)
abstracts-current-label = Abstracts (current)
references-current-label = References (current)
overall-coverage-label = Overall Coverage
overall-impact-label = Overall Impact
earliest-publication-year-label = Earliest publication year
earliest-publication-year-help = Earliest Publication Year
latest-publication-year-label = Latest publication year
latest-publication-year-help = Latest Publication year
2010-total-label = 2010-total
2011-total-label = 2011-total
2012-total-label = 2012-total
2013-total-label = 2013-total
2014-total-label = 2014-total
2015-total-label = 2015-total
2016-total-label = 2016-total
2017-total-label = 2017-total
2018-total-label = 2018-total
2019-total-label = 2019-total
2020-total-label = 2020-total
2021-total-label = 2021-total
avg-pct-change-label = Average % Change PA
display-name-label = Display Name
breakdowns-dois-by-issued-year-label = DOIs registered by year
breakdowns-dois-by-issued-year-help = DOIs registered by year
# New 


all-journal-article-abstracts-label = Abstracts
all-journal-article-affiliations-label = Affiliations
all-journal-article-award-numbers-label = Award numbers
all-journal-article-descriptions-label = Descriptions
all-journal-article-funders-label = Funders
all-journal-article-licenses-label = Licenses
all-journal-article-orcids-label = ORCID iDs
all-journal-article-references-label = References
all-journal-article-resource-links-label = Resource links
all-journal-article-ror-ids-label = RORs
all-journal-article-similarity-checking-label = Similarity checking
all-journal-article-update-policies-label = Update policies


current-journal-article-affiliations-label = Affiliations
current-journal-article-abstracts-label = Abstracts
current-journal-article-orcids-label = ORCID iDs
current-journal-article-licenses-label = Licenses
current-journal-article-references-label = References
current-journal-article-funders-label = Funders
current-journal-article-similarity-checking-label = Similarity Check
current-journal-article-award-numbers-label = Awards
current-journal-article-ror-ids-label = RORs
current-journal-article-update-policies-label = Update policies
current-journal-article-resource-links-label = Resource links
current-journal-article-descriptions-label = Descriptions


backfile-journal-article-abstracts-label = Abstracts
backfile-journal-article-affiliations-label = Affiliations
backfile-journal-article-award-numbers-label = Awards
backfile-journal-article-descriptions-label = Descriptions
backfile-journal-article-funders-label = Funders
backfile-journal-article-licenses-label = Licenses
backfile-journal-article-orcids-label = ORCID iDs
backfile-journal-article-references-label = References
backfile-journal-article-resource-links-label = Resource links
backfile-journal-article-ror-ids-label = RORs
backfile-journal-article-similarity-checking-label = Similarity Checking
backfile-journal-article-update-policies-label = Update policies


title-filter-label = Journal title
title-filter-disabled = Title filter not available

splash-screen =

    Mangosteen

    ![Image](https://crossref.org/img/labs/creature2.svg)
    ### Warnings, Caveats and Weasel Words

    This is a [Crossref Labs](https://www.crossref.org/labs/) version of our production [Particpation reports](https://www.crossref.org/members/prep/).

    It may cause your eyes to bleed.


    ### Why have we done this?

    We never learn.

    ### Why doesn't it do `X`?

    We're glad you asked. The [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) under an MIT Open Source License. We even built it with an eye toward extension. It uses standard libraries like [pandas](https://pandas.pydata.org/) and [Plotly](https://plotly.com/) to do most of its work. And the enitre page is driven by the most excellent [Streamlit](https://streamlit.io/). We look forward to your merge requests!

    ### Can you move the widgets 12 pixels to the right?

    Probably not. Streamlit is fantastic, but it doesn't give you fine-grained control over all UI elements.

    ### What does mangosteen taste like?

    Surprisingly not awful.
    
    ### Like it? Don’t like it? Like bits of it? 
    
    The reason we’re sharing this beta version with you now is that we need feedback. Please!

    You can use [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) report bugs, write up suggestions, and share your thoughts. Thank you!



