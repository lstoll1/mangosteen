my-first-string = Englisch!

loading-data = Daten werden geladen...

splash-screen-heading = Begrüßungsbildschirm

results-screen-heading = Berichte

member-selector-heading = Mitgliederauswahl

member-selector-label = Mitglied auswählen

member-selector-placeholder = Wähle eine Option

member-selector-help = Wählen Sie Ihre bevorzugten Crossref-Mitglieder aus

comment = Neu 

content-filter-heading = Inhaltsfilter

period-filter-label = Zeitraum

period-filter-help = Wählen Sie einen Veröffentlichungszeitraum aus

period-all-label = Alle

period-all-help = Sowohl aktuelle als auch Backfile-Inhalte

period-current-label = Aktuell

period-current-help = Inhalte innerhalb der letzten zwei Jahre

period-backfile-label = Backfile

period-back-file-help = Inhalt zwei Jahre oder älter

content-type-filter-label = Inhaltstyp

content-type-filter-help = Wählen Sie einen Inhaltstyp aus

primary-name-label = Mitgliedsname

primary-name-help = Der Name des Crossref-Mitglieds

primary-resource-logo-label = Logo

primary-resource-logo-help = Das Logo des Mitglieds, wie es von Clearbit abgerufen wurde und auf der primären Ressourcendomäne des Mitglieds basiert.

id-label = Mitgliedsnummer

id-help = Die REST-API-ID des Crossref-Mitglieds

account-type-label = Konto Typ

non-profit-label = Gemeinnützig

date-joined-label = Datum des Beitritts

annual-fee-label = Jahresgebühr

active-label = Aktiv

board-status-label = Vorstandsstatus

board-status-help = Ist das Mitglied ein aktuelles oder ehemaliges Mitglied des Crossref-Vorstands?

current-dois-label = Aktuelle DOIs

current-dois-help = Innerhalb der letzten drei Jahre registrierte DOIs

backfile-dois-label = Backfile-DOIs

backfile-dois-help = DOIs, die vor drei Jahren oder früher registriert wurden

total-dois-label = Gesamt-DOIs

primary-resource-domain-label = Primäre Ressourcendomäne

primary-resource-domain-help = Die Domäne, auf die die DOIs des Mitglieds am häufigsten verweisen

affiliations-current-label = Zugehörigkeiten (aktuell)

similarity-checking-current-label = Ähnlichkeitsprüfung (aktuell)

descriptions-current-label = Beschreibungen (aktuell)

ror-ids-current-label = ROR-IDs (aktuell)

funders-backfile-label = Geldgeber (backfile)

licenses-backfile-label = Lizenzen (Backfile)

funders-current-label = Geldgeber (aktuell)

affiliations-backfile-label = Zugehörigkeiten (aktuell)

resource-links-backfile-label = Ressourcenlinks (Backfile)

orcids-backfile-label = ORCID-iDs (Backfile)

update-policies-current-label = Update-Richtlinien (aktuell)

ror-ids-backfile-label = ROR-IDs (Backfile)

orcids-current-label = ORCID-iDs (aktuell)

similarity-checking-backfile-label = Ähnlichkeitsprüfung (Backfile)

references-backfile-label = Referenzen (backfile)

descriptions-backfile-label = Beschreibungen (Backfile)

award-numbers-backfile-label = Auszeichnungsnummern (Backfile)

update-policies-backfile-label = Update-Richtlinien (Backfile)

licenses-current-label = Lizenzen (aktuell)

award-numbers-current-label = Auszeichnungsnummern (aktuell)

abstracts-backfile-label = Abstracts (Backfile)

resource-links-current-label = Ressourcenlinks (aktuell)

abstracts-current-label = Abstracts (aktuell)

references-current-label = Referenzen (aktuell)

overall-coverage-label = Gesamtabdeckung

overall-impact-label = Gesamtauswirkung

earliest-publication-year-label = Frühestes Erscheinungsjahr

earliest-publication-year-help = Frühestes Erscheinungsjahr

latest-publication-year-label = Letztes Erscheinungsjahr

latest-publication-year-help = Letztes Erscheinungsjahr

2010-total-label =
  2010-insgesamt
  2011-Gesamtlabel = 2011-Gesamt
  2012-Gesamtlabel = 2012-Gesamt
  2013-Gesamtlabel = 2013-Gesamt
  2014-Gesamtlabel = 2014-Gesamt
  2015-Gesamtlabel = 2015-Gesamt
  2016-Gesamtlabel = 2016-Gesamt
  2017-Gesamtlabel = 2017-Gesamt
  2018-Gesamtlabel = 2018-Gesamt
  2019-Gesamtlabel = 2019-Gesamt
  2020-Gesamtlabel = 2020-Gesamt
  2021-Gesamtlabel = 2021-Gesamt

avg-pct-change-label = Durchschnittliche prozentuale Änderung PA

display-name-label = Anzeigename

breakdowns-dois-by-issued-year-label = Registrierte DOIs nach Jahr

breakdowns-dois-by-issued-year-help = Registrierte DOIs nach Jahr

all-journal-article-abstracts-label = Zusammenfassungen

all-journal-article-affiliations-label = Zugehörigkeiten

all-journal-article-award-numbers-label = Auszeichnungsnummern

all-journal-article-descriptions-label = Beschreibungen

all-journal-article-funders-label = Geldgeber

all-journal-article-licenses-label = Lizenzen

all-journal-article-orcids-label = ORCID-IDs

all-journal-article-references-label = Verweise

all-journal-article-resource-links-label = Ressourcenlinks

all-journal-article-ror-ids-label = RORs

all-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

all-journal-article-update-policies-label = Richtlinien aktualisieren

current-journal-article-affiliations-label = Zugehörigkeiten

current-journal-article-abstracts-label = Zusammenfassungen

current-journal-article-orcids-label = ORCID-IDs

current-journal-article-licenses-label = Lizenzen

current-journal-article-references-label = Verweise

current-journal-article-funders-label = Geldgeber

current-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

current-journal-article-award-numbers-label = Auszeichnungen

current-journal-article-ror-ids-label = RORs

current-journal-article-update-policies-label = Richtlinien aktualisieren

current-journal-article-resource-links-label = Ressourcenlinks

current-journal-article-descriptions-label = Beschreibungen

backfile-journal-article-abstracts-label = Zusammenfassungen

backfile-journal-article-affiliations-label = Zugehörigkeiten

backfile-journal-article-award-numbers-label = Auszeichnungen

backfile-journal-article-descriptions-label = Beschreibungen

backfile-journal-article-funders-label = Geldgeber

backfile-journal-article-licenses-label = Lizenzen

backfile-journal-article-orcids-label = ORCID-IDs

backfile-journal-article-references-label = Verweise

backfile-journal-article-resource-links-label = Ressourcenlinks

backfile-journal-article-ror-ids-label = RORs

backfile-journal-article-similarity-checking-label = Ähnlichkeitsprüfung

backfile-journal-article-update-policies-label = Richtlinien aktualisieren

splash-screen =
  Manogsteen
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Warnungen, Vorbehalte und Wieselwörter
  
  Das ist ein [Crossref Labs](https://www.crossref.org/labs/) Version unserer Produktion [Particpation reports](https://www.crossref.org/members/prep/).
  
  Es kann zu Blutungen in den Augen kommen.
  
  
  ### Warum haben wir das getan?
  
  Wir lernen nie.
  
  ### Warum funktioniert „X“ nicht?
  
  Wir freuen uns, dass Sie gefragt haben. Der [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) unter einer MIT Open Source-Lizenz. Wir haben es sogar mit Blick auf die Erweiterung gebaut. Es verwendet Standardbibliotheken wie [pandas](https://pandas.pydata.org/) und [Plotly](https://plotly.com/), um den Großteil seiner Arbeit zu erledigen. Und die gesamte Seite wird von den Besten betreut [Streamlit](https://streamlit.io/). Wir freuen uns auf Ihre Zusammenführungsanfragen!
  
  ### Können Sie die Widgets um 12 Pixel nach rechts verschieben?
  
  Wahrscheinlich nicht. Streamlit ist fantastisch, bietet Ihnen jedoch keine detaillierte Kontrolle über alle Elemente der Benutzeroberfläche.
  
  ### Wie schmeckt Mangostan?
  
  Überraschenderweise nicht schrecklich.
  
  ### Mag ich? Gefällt es Ihnen nicht? Mögen Sie Teile davon?
  
  Der Grund, warum wir diese Betaversion jetzt mit Ihnen teilen, ist, dass wir Feedback benötigen. Bitte!
  
  Sie können verwenden [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) Melden Sie Fehler, schreiben Sie Vorschläge und teilen Sie Ihre Gedanken. Danke schön!

