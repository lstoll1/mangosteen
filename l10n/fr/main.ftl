my-first-string = Anglais!

loading-data = Chargement des données...

splash-screen-heading = Écran de démarrage

results-screen-heading = Rapports

member-selector-heading = Sélecteur de membres

member-selector-label = Sélectionner un membre

member-selector-placeholder = Choisis une option

member-selector-help = Sélectionnez vos membres Crossref préférés

comment = Nouveau 

content-filter-heading = Filtre de contenu

period-filter-label = Période de temps

period-filter-help = Sélectionnez une période de publication

period-all-label = Tous

period-all-help = Contenu actuel et rétrospectif

period-current-label = Actuel

period-current-help = Contenu au cours des deux dernières années

period-backfile-label = Retour en arrière

period-back-file-help = Contenu de deux ans ou plus

content-type-filter-label = Type de contenu

content-type-filter-help = Sélectionnez un type de contenu

primary-name-label = Nom de membre

primary-name-help = Le nom du membre Crossref

primary-resource-logo-label = Logo

primary-resource-logo-help = Le logo du membre extrait de Clearbit et basé sur le domaine de ressources principal du membre.

id-label = ID membres

id-help = L'ID de l'API REST du membre Crossref

account-type-label = Type de compte

non-profit-label = Non lucratif

date-joined-label = Date d'adhésion

annual-fee-label = Frais annuels

active-label = Actif

board-status-label = Statut du conseil d'administration

board-status-help = Le membre est-il un membre actuel ou passé du conseil d’administration de Crossref ?

current-dois-label = Current DOIs

current-dois-help = DOI enregistrés au cours des trois dernières années

backfile-dois-label = Rétroarchiver les DOI

backfile-dois-help = DOI enregistrés il y a trois ans ou avant

total-dois-label = Nombre total de DOI

primary-resource-domain-label = Domaine de ressources principal

primary-resource-domain-help = Le domaine vers lequel les DOI du membre pointent le plus souvent

affiliations-current-label = Affiliations (actuelles)

similarity-checking-current-label = Vérification de similarité (actuel)

descriptions-current-label = Descriptions (actuelles)

ror-ids-current-label = ID ROR (actuels)

funders-backfile-label = Bailleurs de fonds (fichier rétrospectif)

licenses-backfile-label = Licences (backfile)

funders-current-label = Bailleurs de fonds (actuels)

affiliations-backfile-label = Affiliations (actuelles)

resource-links-backfile-label = Liens vers les ressources (backfile)

orcids-backfile-label = ORCID identifiants (backfile)

update-policies-current-label = Politiques de mise à jour (actuelles)

ror-ids-backfile-label = ID ROR (backfile)

orcids-current-label = ORCID identifiants (actuels)

similarity-checking-backfile-label = Vérification de similarité (backfile)

references-backfile-label = Références (backfile)

descriptions-backfile-label = Descriptions (fichier rétrospectif)

award-numbers-backfile-label = Numéros de récompense (fichier rétrospectif)

update-policies-backfile-label = Mettre à jour les politiques (backfile)

licenses-current-label = Licences (actuelles)

award-numbers-current-label = Numéros de récompense (actuels)

abstracts-backfile-label = Résumés (backfile)

resource-links-current-label = Liens vers les ressources (actuels)

abstracts-current-label = Résumés (actuels)

references-current-label = Références (actuelles)

overall-coverage-label = Couverture globale

overall-impact-label = Impact global

earliest-publication-year-label = Première année de publication

earliest-publication-year-help = Année de publication la plus ancienne

latest-publication-year-label = Dernière année de publication

latest-publication-year-help = Dernière année de publication

2010-total-label =
  2010-total
  Étiquette-total-2011 = Total-2011
  Label-total-2012 = Total-2012
  Label-total-2013 = Total-2013
  Label-total-2014 = Total-2014
  Label-total-2015 = Total-2015
  Label-total-2016 = Total-2016
  Label-total-2017 = Total-2017
  Label-total-2018 = Total-2018
  Label-total-2019 = Total-2019
  Label-total-2020 = Total-2020
  Label-total-2021 = Total-2021

avg-pct-change-label = % de changement moyen PA

display-name-label = Afficher un nom

breakdowns-dois-by-issued-year-label = DOI enregistrés par année

breakdowns-dois-by-issued-year-help = DOI enregistrés par année

all-journal-article-abstracts-label = Résumés

all-journal-article-affiliations-label = Affiliations

all-journal-article-award-numbers-label = Numéros de récompense

all-journal-article-descriptions-label = Descriptions

all-journal-article-funders-label = Bailleurs de fonds

all-journal-article-licenses-label = Licences

all-journal-article-orcids-label = ORCID identifiants

all-journal-article-references-label = Les références

all-journal-article-resource-links-label = Liens vers les ressources

all-journal-article-ror-ids-label = ROR

all-journal-article-similarity-checking-label = Vérification de similarité

all-journal-article-update-policies-label = Mettre à jour les politiques

current-journal-article-affiliations-label = Affiliations

current-journal-article-abstracts-label = Résumés

current-journal-article-orcids-label = ORCID identifiants

current-journal-article-licenses-label = Licences

current-journal-article-references-label = Les références

current-journal-article-funders-label = Bailleurs de fonds

current-journal-article-similarity-checking-label = Vérification de similarité

current-journal-article-award-numbers-label = Prix

current-journal-article-ror-ids-label = ROR

current-journal-article-update-policies-label = Mettre à jour les politiques

current-journal-article-resource-links-label = Liens vers les ressources

current-journal-article-descriptions-label = Descriptions

backfile-journal-article-abstracts-label = Résumés

backfile-journal-article-affiliations-label = Affiliations

backfile-journal-article-award-numbers-label = Prix

backfile-journal-article-descriptions-label = Descriptions

backfile-journal-article-funders-label = Bailleurs de fonds

backfile-journal-article-licenses-label = Licences

backfile-journal-article-orcids-label = ORCID identifiants

backfile-journal-article-references-label = Les références

backfile-journal-article-resource-links-label = Liens vers les ressources

backfile-journal-article-ror-ids-label = ROR

backfile-journal-article-similarity-checking-label = Vérification de similarité

backfile-journal-article-update-policies-label = Mettre à jour les politiques

splash-screen =
  Manogsteen
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Avertissements, mises en garde et mots de fouine
  
  C'est un [Crossref Labs](https://www.crossref.org/labs/) version de notre production [Particpation reports](https://www.crossref.org/members/prep/).
  
  Cela pourrait faire saigner vos yeux.
  
  
  ### Pourquoi avons-nous fait cela ?
  
  Nous n'apprenons jamais.
  
  ### Pourquoi ne fait-il pas « X » ?
  
  Nous sommes heureux que vous ayez posé la question. Le [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) sous licence Open Source MIT. Nous l’avons même construit dans un souci d’extension. Il utilise des bibliothèques standards comme [pandas](https://pandas.pydata.org/) et [Plotly](https://plotly.com/) pour faire l'essentiel de son travail. Et toute la page est pilotée par les plus excellents [Streamlit](https://streamlit.io/). Nous attendons avec impatience vos demandes de fusion !
  
  ### Pouvez-vous déplacer les widgets de 12 pixels vers la droite ?
  
  Probablement pas. Streamlit est fantastique, mais il ne vous donne pas un contrôle précis sur tous les éléments de l'interface utilisateur.
  
  ### Quel goût a le mangoustan ?
  
  Étonnamment, pas terrible.
  
  ### J'aime ça? Vous n'aimez pas ça ? Vous en aimez des morceaux ?
  
  La raison pour laquelle nous partageons cette version bêta avec vous maintenant est que nous avons besoin de commentaires. S'il te plaît!
  
  Vous pouvez utiliser [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) signalez les bugs, rédigez des suggestions et partagez vos réflexions. Merci!

