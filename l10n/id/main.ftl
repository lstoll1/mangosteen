my-first-string = Bahasa inggris!

loading-data = Memuat data...

splash-screen-heading = Layar Pembuka

results-screen-heading = Laporan

member-selector-heading = Pemilih anggota

member-selector-label = Pilih anggota

member-selector-placeholder = Pilih satu opsi

member-selector-help = Pilih anggota Crossref favorit Anda

comment = Baru 

content-filter-heading = Penyaring konten

period-filter-label = Jangka waktu

period-filter-help = Pilih jangka waktu publikasi

period-all-label = Semua

period-all-help = Baik konten saat ini maupun konten backfile

period-current-label = Saat ini

period-current-help = Konten dalam dua tahun terakhir

period-backfile-label = File belakang

period-back-file-help = Konten dua tahun atau lebih

content-type-filter-label = Jenis konten

content-type-filter-help = Pilih tipe konten

primary-name-label = Nama anggota

primary-name-help = Nama anggota Crossref

primary-resource-logo-label = Logo

primary-resource-logo-help = Logo anggota diambil dari Clearbit dan berdasarkan domain sumber daya utama anggota.

id-label = Tanda Anggota

id-help = ID REST API anggota Crossref

account-type-label = Jenis akun

non-profit-label = Nirlaba

date-joined-label = Tanggal Bergabung

annual-fee-label = Biaya tahunan

active-label = Aktif

board-status-label = Status Dewan

board-status-help = Apakah anggota tersebut merupakan anggota dewan Crossref saat ini atau sebelumnya?

current-dois-label = DOI saat ini

current-dois-help = DOI yang didaftarkan dalam tiga tahun terakhir

backfile-dois-label = DOI file belakang

backfile-dois-help = DOI didaftarkan tiga tahun lalu atau lebih awal

total-dois-label = Jumlah DOI

primary-resource-domain-label = Domain sumber daya utama

primary-resource-domain-help = Domain yang paling sering ditunjuk oleh DOI anggota

affiliations-current-label = Afiliasi (saat ini)

similarity-checking-current-label = Pemeriksaan Kesamaan (saat ini)

descriptions-current-label = Deskripsi (saat ini)

ror-ids-current-label = ID ROR (saat ini)

funders-backfile-label = Pemberi dana (file belakang)

licenses-backfile-label = Lisensi (file belakang)

funders-current-label = Pemberi dana (saat ini)

affiliations-backfile-label = Afiliasi (saat ini)

resource-links-backfile-label = Tautan Sumber Daya (file belakang)

orcids-backfile-label = iD ORCID (file belakang)

update-policies-current-label = Perbarui Kebijakan (saat ini)

ror-ids-backfile-label = ID ROR (file belakang)

orcids-current-label = iD ORCID (saat ini)

similarity-checking-backfile-label = Pemeriksaan Kesamaan (backfile)

references-backfile-label = Referensi (file belakang)

descriptions-backfile-label = Deskripsi (file belakang)

award-numbers-backfile-label = Nomor Penghargaan (file belakang)

update-policies-backfile-label = Kebijakan Pembaruan (file belakang)

licenses-current-label = Lisensi (saat ini)

award-numbers-current-label = Nomor Penghargaan (saat ini)

abstracts-backfile-label = Abstrak (file belakang)

resource-links-current-label = Tautan Sumber Daya (saat ini)

abstracts-current-label = Abstrak (saat ini)

references-current-label = Referensi (saat ini)

overall-coverage-label = Cakupan Keseluruhan

overall-impact-label = Dampak Keseluruhan

earliest-publication-year-label = Tahun penerbitan paling awal

earliest-publication-year-help = Tahun Penerbitan Paling Awal

latest-publication-year-label = Tahun penerbitan terakhir

latest-publication-year-help = Tahun Publikasi Terbaru

2010-total-label =
  2010-total
  Label total 2011 = total 2011
  Label total 2012 = total 2012
  Label total 2013 = total 2013
  Label total 2014 = total 2014
  Label total 2015 = total 2015
  Label total 2016 = total 2016
  Label total 2017 = total 2017
  Label total 2018 = total 2018
  Label total 2019 = total 2019
  Label total 2020 = total 2020
  Label total 2021 = total 2021

avg-pct-change-label = Rata-rata % Perubahan PA

display-name-label = Nama tampilan

breakdowns-dois-by-issued-year-label = DOI didaftarkan berdasarkan tahun

breakdowns-dois-by-issued-year-help = DOI didaftarkan berdasarkan tahun

all-journal-article-abstracts-label = Abstrak

all-journal-article-affiliations-label = Afiliasi

all-journal-article-award-numbers-label = Nomor penghargaan

all-journal-article-descriptions-label = Deskripsi

all-journal-article-funders-label = Pemberi dana

all-journal-article-licenses-label = Lisensi

all-journal-article-orcids-label = iD ORCID

all-journal-article-references-label = Referensi

all-journal-article-resource-links-label = Tautan sumber daya

all-journal-article-ror-ids-label = ROR

all-journal-article-similarity-checking-label = Pemeriksaan kesamaan

all-journal-article-update-policies-label = Perbarui kebijakan

current-journal-article-affiliations-label = Afiliasi

current-journal-article-abstracts-label = Abstrak

current-journal-article-orcids-label = iD ORCID

current-journal-article-licenses-label = Lisensi

current-journal-article-references-label = Referensi

current-journal-article-funders-label = Pemberi dana

current-journal-article-similarity-checking-label = Pemeriksaan Kemiripan

current-journal-article-award-numbers-label = Penghargaan

current-journal-article-ror-ids-label = ROR

current-journal-article-update-policies-label = Perbarui kebijakan

current-journal-article-resource-links-label = Tautan sumber daya

current-journal-article-descriptions-label = Deskripsi

backfile-journal-article-abstracts-label = Abstrak

backfile-journal-article-affiliations-label = Afiliasi

backfile-journal-article-award-numbers-label = Penghargaan

backfile-journal-article-descriptions-label = Deskripsi

backfile-journal-article-funders-label = Pemberi dana

backfile-journal-article-licenses-label = Lisensi

backfile-journal-article-orcids-label = iD ORCID

backfile-journal-article-references-label = Referensi

backfile-journal-article-resource-links-label = Tautan sumber daya

backfile-journal-article-ror-ids-label = ROR

backfile-journal-article-similarity-checking-label = Pemeriksaan Kesamaan

backfile-journal-article-update-policies-label = Perbarui kebijakan

splash-screen =
  Manggis
  
  ![Image](https://crossref.org/img/labs/creature2.svg)
  ### Peringatan, Peringatan dan Kata Musang
  
  Ini adalah sebuah [Crossref Labs](https://www.crossref.org/labs/) versi produksi kami [Particpation reports](https://www.crossref.org/members/prep/).
  
  Hal ini dapat menyebabkan mata Anda berdarah.
  
  
  ### Mengapa kami melakukan ini?
  
  Kami tidak pernah belajar.
  
  ### Mengapa tidak melakukan `X`?
  
  Kami senang Anda bertanya. Itu [source is available in GitLab](https://gitlab.com/crossref/labs/mangosteen) di bawah Lisensi Sumber Terbuka MIT. Kami bahkan membangunnya dengan tujuan perluasan. Ini menggunakan perpustakaan standar seperti [pandas](https://pandas.pydata.org/) dan [Plotly](https://plotly.com/) untuk melakukan sebagian besar pekerjaannya. Dan seluruh halaman didorong oleh yang paling bagus [Streamlit](https://streamlit.io/). Kami menantikan permintaan penggabungan Anda!
  
  ### Bisakah Anda memindahkan widget 12 piksel ke kanan?
  
  Mungkin tidak. Streamlit memang luar biasa, tetapi tidak memberi Anda kendali penuh atas semua elemen UI.
  
  ### Seperti apa rasanya manggis?
  
  Anehnya tidak buruk.
  
  ### Suka itu? Tidak menyukainya? Suka sebagian?
  
  Alasan kami membagikan versi beta ini kepada Anda sekarang adalah karena kami memerlukan masukan. Silakan!
  
  Anda dapat gunakan [Gitlab Issues](https://gitlab.com/crossref/labs/mangosteen/-/issues) laporkan bug, tulis saran, dan bagikan pemikiran Anda. Terima kasih!

