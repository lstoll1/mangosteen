from mangosteen.hookspecs import hookimpl
from mangosteen.column_definitions import column_config
from streamlit import container
from pandas import DataFrame
import streamlit as st

OVERVIEW_COLUMNS = [
    "primary-name",
    "id",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-status",
    "earliest-publication-year",
    "latest-publication-year",
    "current-dois",
    "backfile-dois",
    "total-dois",
    "avg-pct-change",
]


class Overview:
    def invert_dataframe(self, df: DataFrame):
        column_names = df.columns.tolist()
        column_names[0], column_names[1] = column_names[1], column_names[0]
        new_df = df[column_names]
        new_df = new_df.set_index("primary-name")
        new_df = new_df.T
        return new_df

    @hookimpl
    def title(self):
        return "Overview"

    @hookimpl
    def description(self):
        return "Overview of member data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading overview for {source}"

    @hookimpl
    def display_data(self, container: container, data: DataFrame, shared_state: dict):
        selected_member_ids = [int(id) for id in shared_state["selected-member-ids"]]
        filtered_data = data[data["id"].isin(selected_member_ids)]
        filtered_data = filtered_data[OVERVIEW_COLUMNS]

        container.dataframe(
            filtered_data.head(10),
            hide_index=True,
            use_container_width=True,
            column_config=column_config(filtered_data),
        )
