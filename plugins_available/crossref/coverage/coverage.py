from mangosteen.hookspecs import hookimpl
import streamlit as st
from streamlit import container
from pandas import DataFrame

# from mangosteen.column_definitions import get_column_config, coverage_columns
from mangosteen.gridify import gridify, create_grid_spec
from mangosteen.sessions import get_shared_session_state, currently_selected_member_ids
from mangosteen.data import journal_name_to_id_map, load_member_data, load_title_data

# MEMBER_COMMON_COLUMNS = ["primary-name", "primary-resource-logo"]
MEMBER_COMMON_COLUMNS = ["primary-name"]

MEMBER_COVERAGE_COLUMNS = [
    "abstracts-backfile",
    "abstracts-current",
    "affiliations-backfile",
    "affiliations-current",
    "award-numbers-backfile",
    "award-numbers-current",
    "descriptions-backfile",
    "descriptions-current",
    "funders-backfile",
    "funders-current",
    "licenses-backfile",
    "licenses-current",
    "orcids-backfile",
    "orcids-current",
    "overall-coverage",
    "overall-impact",
    "references-backfile",
    "references-current",
    "resource-links-backfile",
    "resource-links-current",
    "ror-ids-backfile",
    "ror-ids-current",
    "similarity-checking-backfile",
    "similarity-checking-current",
    "update-policies-backfile",
    "update-policies-current",
]

TITLE_COMMON_COLUMNS = ["title"]


def coverage_column_filter():
    selected_period = get_shared_session_state("selected-period")
    selected_content_type = get_shared_session_state("selected-content-type")
    return f"{selected_period}-{selected_content_type}"


def coverage_columns():
    return sorted(
        [
            column
            for column in load_member_data().columns
            if (
                column.startswith(coverage_column_filter())
                and not column.endswith("status-check-time")
            )
        ]
    )


def display_member_data():
    st.success("Displaying coverage data for all member titles")
    data = load_member_data()

    selected_member_ids = currently_selected_member_ids()
    filtered_data = data[data["id"].isin(selected_member_ids)]

    columns_to_display = MEMBER_COMMON_COLUMNS + coverage_columns()
    filtered_data = filtered_data[columns_to_display]

    grid_spec = create_grid_spec(len(columns_to_display), len(selected_member_ids))

    gridify(
        filtered_data,
        pivot_column="primary-name",
        grid_spec=grid_spec,
    )


def display_title_data(self, data: DataFrame):
    selected_title = get_shared_session_state("selected-title")
    selected_title_id = journal_name_to_id_map().get(selected_title, None)

    st.warning(
        f'Displaying coverage data for the title, "{selected_title}" ({selected_title_id})'
    )
    data = load_title_data()

    filtered_data = data[data["pissn"].isin([selected_title_id])]

    columns_to_display = TITLE_COMMON_COLUMNS + coverage_columns()
    filtered_data = filtered_data[columns_to_display]

    grid_spec = create_grid_spec(len(columns_to_display), 1)

    gridify(
        filtered_data,
        pivot_column="title",
        grid_spec=grid_spec,
    )


class Coverage:
    @hookimpl
    def title(self):
        return "Coverage"

    @hookimpl
    def description(self):
        return "Coverage of member data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def show_content_type_filter(self) -> bool:
        return True

    @hookimpl
    def load_data(self, source: str):
        return f"Loading coverage for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        selected_title = get_shared_session_state("selected-title")
        selected_title_id = journal_name_to_id_map().get(selected_title, None)

        # st.subheader(f"Filter is: {coverage_column_filter()}")
        # st.subheader(f"Selected container title is: {selected_title}")
        # st.subheader(f"Selected title id is: {selected_title_id}")

        if selected_title_id:
            display_title_data(self, data)
        else:
            display_member_data()
