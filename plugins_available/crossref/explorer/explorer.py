from mangosteen.hookspecs import hookimpl
from mangosteen.column_definitions import column_config

from streamlit import container
from streamlit_extras.dataframe_explorer import dataframe_explorer

from pandas import DataFrame


class Explorer:
    @hookimpl
    def title(self):
        return "Data Explorer"

    @hookimpl
    def description(self):
        return "Filter and browse data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading counts for {source}"

    @hookimpl
    def display_data(self, container: container, data: DataFrame, shared_state: dict):
        container.write("Exploring data")

        # filtered_df = dataframe_explorer(data, case=False)
        # # container.dataframe(
        # #     filtered_df, use_container_width=True, column_config=column_config()
        # # )
        # container.dataframe(filtered_df, use_container_width=True)
