from mangosteen.hookspecs import hookimpl

# from mangosteen.column_definitions import column_config
from streamlit import container
from pandas import DataFrame
from mangosteen.gridify import gridify, create_grid_spec, make_grid
from mangosteen.sessions import get_shared_session_state, currently_selected_member_ids

OVERVIEW_COLUMNS = [
    "primary-name",
    "primary-resource-logo",
    "id",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-status",
    "earliest-publication-year",
    "latest-publication-year",
    "current-dois",
    "backfile-dois",
    "total-dois",
    "primary-resource-domain",
    "breakdowns-dois-by-issued-year",
]


class Gridoverview:
    # def invert_dataframe(self, df: DataFrame):
    #     column_names = df.columns.tolist()
    #     column_names[0], column_names[1] = column_names[1], column_names[0]
    #     new_df = df[column_names]
    #     new_df = new_df.set_index("primary-name")
    #     new_df = new_df.T
    #     return new_df

    @hookimpl
    def title(self):
        return "Overview"

    @hookimpl
    def description(self):
        return "Grid Overview of member data"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def load_data(self, source: str):
        return f"Loading overview for {source}"

    @hookimpl
    def display_data(self, data: DataFrame):
        selected_member_ids = currently_selected_member_ids()
        filtered_data = data[data["id"].isin(selected_member_ids)]

        filtered_data = filtered_data[OVERVIEW_COLUMNS]

        grid_spec = create_grid_spec(len(OVERVIEW_COLUMNS), len(selected_member_ids))

        gridify(
            filtered_data,
            pivot_column="primary-name",
            grid_spec=grid_spec,
        )
