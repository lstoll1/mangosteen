from mangosteen.hookspecs import hookimpl
from streamlit import container
from pandas import DataFrame


class Counts():

    @hookimpl
    def title(self):
        return "Counts"


    @hookimpl
    def description(self):
        return "Counts of member data"


    @hookimpl
    def version(self):
        return "1.0.0"


    @hookimpl
    def load_data(self, source: str):
        return f"Loading counts for {source}"


    @hookimpl
    def display_data(self, container: container, data: DataFrame, shared_state: dict):
        container.write("Displaying counts data")
