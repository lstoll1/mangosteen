from mangosteen.hookspecs import hookimpl
from streamlit import container
from pandas import DataFrame

OVERVIEW_COLUMNS = [
    "primary-name",
    "id",
    "account-type",
    "non-profit",
    "date-joined",
    "annual-fee",
    "active",
    "board-status",
    "earliest-publication-year",
    "latest-publication-year",
    "current-dois",
    "backfile-dois",
    "total-dois",
    "avg-pct-change",
]


class Piglatin:
    def piglatin(self, sentence: str) -> str:
        """Return the piglatin version of a sentence"""
        words = sentence.split()
        new_words = []
        for word in words:
            if word[0] in "aeiou":
                new_words.append(f"{word}way")
            else:
                new_words.append(word[1:] + word[0] + "ay")
        return " ".join(new_words)

    @hookimpl
    def title(self):
        return "Pig Latin"

    @hookimpl
    def description(self):
        return "Translate member name to Pig Latin"

    @hookimpl
    def version(self):
        return "1.0.0"

    @hookimpl
    def show_content_type_filter(self) -> bool:
        return True

    @hookimpl
    def display_data(self, container: container, data: DataFrame, shared_state: dict):
        container.write(self.piglatin(self.description()))

        selected_member_ids = [int(id) for id in shared_state["selected-member-ids"]]

        filtered_data = data[data["id"].isin(selected_member_ids)]
        filtered_data = filtered_data[OVERVIEW_COLUMNS]
        columns = container.columns(len(shared_state["selected-member-ids"]))
        for index, column in enumerate(columns):
            column.header(f"Member: {selected_member_ids[index]}")

        # col1, col2, col3 = container.columns([1, 1, 1])

        # col1.header("A cat")
        # col1.image("https://static.streamlit.io/examples/cat.jpg")

        # col2.header("A dog")
        # col2.image("https://static.streamlit.io/examples/dog.jpg")

        # col3.header("An owl")
        # col3.image("https://static.streamlit.io/examples/owl.jpg")
