#!/usr/bin/env bash
fly deploy --local-only  --build-arg COMMIT=$(git rev-parse HEAD)