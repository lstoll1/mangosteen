import streamlit as st
import pandas as pd
from streamlit_extras.dataframe_explorer import dataframe_explorer


@st.cache_data(show_spinner="Loading journal data")
def journal_data():
    return pd.read_parquet("data/journals.parquet")


st.title("PRVPRQ")
# create dataframe from parquette file
st.header("Journals")
st.selectbox(
    "Select a journal",
    ["Journal of the American Medical Association", "New England Journal of Medicine"],
)

df = journal_data()
filtered_df = dataframe_explorer(df, case=False)
st.dataframe(filtered_df)

st.write(df.dtypes)
