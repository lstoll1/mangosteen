import datetime
import json
import logging
import urllib
from collections import OrderedDict
from pathlib import Path

import ijson
import pandas as pd
import typer

# Columns we want stored as ints
COUNT_COLUMNS = ["total-dois", "current-dois", "backfile-dois"]
YEAR_COLUMNS = ["earliest-publication-year", "latest-publication-year"]

# Prefixes of columns we want stored as floats (unless they are counts)
FLOAT_PREFIXES = ("all-", "current-", "backfile-")

MEMBER_ERROR_URL_TEMPLATE = (
    "https://api.labs.crossref.org/members/{member_id}?mailto=labs@crossref.org"
)

#############################################################################
# Calculate current board status
# TODO move into Labs API
#############################################################################
BOARD_MEMBERS = pd.read_csv("data/board_ids.csv")
BOARD_STATUS = {
    id: "current"
    for id in BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "current"]["id"].tolist()
}
BOARD_STATUS.update(
    {
        id: "past"
        for id in BOARD_MEMBERS.loc[BOARD_MEMBERS["status"] == "past"]["id"].tolist()
    }
)


def all_records(fn):
    with open(fn, "rb") as f:
        yield from ijson.items(f, "message.items.item")


def board_status(member_id: int) -> str:
    return BOARD_STATUS.get(member_id, "none")


def xvalid_member_record(member):
    try:
        total_dois = member["counts"]["total-dois"]
        account_type = member["cr-labs-member-profile"]["account-type"]

        return total_dois > 0 and account_type != "Uncategorized"
    except KeyError as e:
        logging.error(
            f"Error sense checking member {member['id']}: a problem with key {e}"
        )
        return False


def zvalid_member_record(member):
    member_id = member["id"]
    url = MEMBER_ERROR_URL_TEMPLATE.format(member_id=member_id)
    counts = member.get("counts", {})
    if counts == {}:
        logging.error(f"no counts key: {url}")
        return False
    total_dois = counts.get("total-dois", -1)
    if total_dois < 0:
        logging.error(f"no total-dois key: {url}")
        return False
    member_profile = member.get("cr-labs-member-profile", {})
    if not member_profile:
        logging.error(f"no member-profile key: {url}")
        return False
    account_type = member_profile.get("account-type", {})
    if account_type == {}:
        logging.error(f"no account-type key: {url}")
        return False
    if account_type == "":
        logging.warning(f"account-type is Uncategorized: {url}")

    return True


def valid_member_record(member):
    member_id = member["id"]
    url = MEMBER_ERROR_URL_TEMPLATE.format(member_id=member_id)

    required_keys = (
        ("counts", "total-dois"),
        ("cr-labs-member-profile", "account-type"),
    )

    missing_keys = [
        f"{key}.{subkey}"
        for key, subkey in required_keys
        if key not in member or subkey not in member[key]
    ]
    if missing_keys:
        logging.error(f"Missing or invalid keys: {', '.join(missing_keys)} - {url}")
        return False

    account_type = member["cr-labs-member-profile"]["account-type"]
    if account_type == "":
        logging.warning(f"account-type is Uncategorized: {url}")

    return True


def valid_journal_record(journal):
    counts = journal.get("counts", {})
    total_dois = counts.get("total-dois", 0)
    member_id = journal.get("cr-labs-member-id", {}).get("member-id", None)
    if total_dois > 0 and member_id is not None:
        return True

    title = journal.get("title", None)
    encoded_title = urllib.parse.quote_plus(title)
    url = f"https://api.labs.crossref.org/journals?query={encoded_title}"
    if total_dois == 0:
        logging.error(f"Zero DOIs: {url}")
    if not member_id:
        logging.error(f"Missing member ID: {url}")

    return False


def calculate_active_years(member: dict) -> tuple:
    """extract all the years from breakdowns, return tuple of first & last years"""
    publishing_years = [
        entry[0]
        for entry in member["breakdowns"]["dois-by-issued-year"]
        if entry[1] > 0
    ] or [datetime.datetime.now().year + 1]
    return (min(publishing_years), max(publishing_years))


def annual_breakdowns(member):
    val = member["breakdowns"]["dois-by-issued-year"]
    return json.dumps(
        {
            "years": [item[0] for item in val],
            "counts": [item[1] for item in val],
        }
    )


def tlds(member):
    url = MEMBER_ERROR_URL_TEMPLATE.format(member_id=member["id"])
    domains = member.get("cr-labs-domains", {}).get("domains", [])
    if not domains:
        logging.warning(f"No domains listed for {url}")
    return [domain["value"] for domain in domains]


def logo_url(domain):
    return f"https://logo.clearbit.com/{domain}"


def summarize_member(member):
    logging.debug(f"Summarizing {member['id']}")
    try:
        most_common_tlds = tlds(member)
        primary_tld = most_common_tlds[0] if most_common_tlds else "crossref.org"

        earliest_publication_year, latest_publication_year = calculate_active_years(
            member
        )

        summary = (
            {
                "primary-resource-logo": logo_url(primary_tld),
                "id": member["id"],
                "primary-name": member["primary-name"],
                "display-name": member["primary-name"].replace("&quot;", ""),
                "account-type": member["cr-labs-member-profile"]["account-type"],
                "non-profit": member["cr-labs-member-profile"]["non-profit"],
                "date-joined": member["cr-labs-member-profile"]["date-joined"],
                "annual-fee": member["cr-labs-member-profile"]["annual-fee"],
                "active": member["cr-labs-member-profile"]["inactive"] != "Yes",
                "board-status": board_status(member["id"]),
                "breakdowns-dois-by-issued-year": annual_breakdowns(member),
                "earliest-publication-year": earliest_publication_year,
                "latest-publication-year": latest_publication_year,
                "primary-resource-domain": primary_tld,
                "resource-tlds": most_common_tlds,
            }
            | member["counts"]
            | member["coverage-type"]
        )

    except Exception as e:
        logging.error(f"Error summarizing {member['id']}: {e}")
        raise e
    return summary


def get_crcid(journal):
    return journal.get("cr-labs-container-ids", {}).get("crcid", None)


def issns(journal):
    # Unpacking the list into variables pissn and eissn
    # default to None if there is no corresponding issn
    pissn, eissn = (journal.get("ISSN", []) + [None, None])[:2]
    return pissn, eissn


# Annoyingly, REST API's the journals route displays coverage data
# in a slightly different format than the members route.
# This standardizes the format so that we don't have
# to have separate display logic scattered throughout the
# reports code.
def normalise_coverage_type(coverage_type):
    return {
        period: {"journal-article": record} for period, record in coverage_type.items()
    }


def summarize_journal(journal):
    pissn, eissn = issns(journal)

    return (
        {
            "crcid": get_crcid(journal),
            "title": journal["title"],
            "pissn": pissn,
            "eissn": eissn,
            "member-id": journal.get("cr-labs-member-id", {}).get("member-id", None),
        }
        | journal["counts"]
        | normalise_coverage_type(journal["coverage-type"])
    )


def convert_to_int(df, columns):
    df[columns] = df[columns].astype("int32")
    return df


def is_coverage_column(column):
    return column.startswith(FLOAT_PREFIXES) and column not in COUNT_COLUMNS


def coverage_columns(df):
    return [column for column in df.columns if is_coverage_column(column)]


def convert_to_float(df):
    for column in coverage_columns(df):
        df[column] = df[column].astype("float64")
    return df


def date_from_unix_timestamp(df):
    columns = [column for column in df.columns if column.endswith("status-check-time")]
    for column in columns:
        df[column] = pd.to_datetime(df[column], unit="ms")
    return df


def cleanup_members_dataframe(df):
    df = df.fillna(0)
    df = convert_to_int(df, COUNT_COLUMNS + YEAR_COLUMNS + ["id"])
    df = convert_to_float(df)
    df = date_from_unix_timestamp(df)
    df["account-type"] = df["account-type"].replace("", "Uncategorized")
    return df


def cleanup_journals_dataframe(df):
    # TODO complete this
    df = convert_to_int(df, COUNT_COLUMNS)
    df = convert_to_float(df)
    df = date_from_unix_timestamp(df)
    return df


def summarize_members(fn):
    return [
        summarize_member(member)
        for member in all_records(fn)
        if valid_member_record(member)
    ]


def summarize_journals(fn):
    return [
        summarize_journal(journal)
        for journal in all_records(fn)
        if valid_journal_record(journal)
    ]


def convert_members_to_df(summaries):
    df = pd.json_normalize(summaries, sep="-")
    df = cleanup_members_dataframe(df)
    return df


def convert_journals_to_df(summaries):
    df = pd.json_normalize(summaries, sep="-")
    df = cleanup_journals_dataframe(df)
    return df


def save_to_parquet(df, path):
    df.to_parquet(path)


def save_to_json(data, path):
    with open(path, "w") as f:
        json.dump(data, f)


def member_name_to_id_map(df):
    df = df.sort_values(by=["display-name"])
    return OrderedDict(zip(df["display-name"], df["id"]))


def member_id_to_name_map(df):
    # return a dict with the key as ints from the id column and the value as the display-name
    return {int(v): k for k, v in member_name_to_id_map(df).items()}


def prepare_member_data(fn):
    df = convert_members_to_df(summarize_members(fn))
    save_to_parquet(df, "data/members.parquet")
    save_to_json(member_name_to_id_map(df), "data/member_name_to_id_map.json")
    save_to_json(member_id_to_name_map(df), "data/member_id_to_name_map.json")
    logging.info(f"Converted {len(df)} members to parquet")


def journal_name_to_id_map(df):
    df = df.sort_values(by=["title"])
    ## Here
    return OrderedDict(zip(df["title"], df["pissn"]))


def journal_id_to_name_map(df):
    return {v: k for k, v in journal_name_to_id_map(df).items()}


def prepare_journals_data(fn):
    df = convert_journals_to_df(summarize_journals(fn))
    save_to_parquet(df, "data/journals.parquet")
    save_to_json(journal_name_to_id_map(df), "data/journal_name_to_id_map.json")
    save_to_json(journal_id_to_name_map(df), "data/journal_id_to_name_map.json")
    logging.info(f"Converted {len(df)} journals to parquet")


def sniff_data_type(fn):
    with open(fn, "rb") as f:
        return next(ijson.items(f, "message-type"))


def main(
    file_name: Path = typer.Option("members.json", "--file", "-f", exists=True),
    verbose: bool = typer.Option(False, "--verbose", "-v"),
):
    log_levels = {True: logging.INFO, False: logging.WARNING}
    logging.basicConfig(level=log_levels[verbose])

    data_type_handlers = {
        "member-list": prepare_member_data,
        "journal-list": prepare_journals_data,
    }
    data_type = sniff_data_type(file_name)
    try:
        logging.info(f"Processing {data_type} data")
        data_type_handlers[data_type](file_name)
    except KeyError as e:
        logging.error(f"Unknown data type {data_type}")
        raise e


if __name__ == "__main__":
    typer.run(main)
