import asyncio


def example():
    return "example text"


async def await_me_maybe(value):
    """
    The `await_me_maybe` function checks if the input value is callable or awaitable, and if so, calls
    or awaits it respectively; otherwise, it returns the value as is.

    :param value: The parameter `value` can be any object. It can be a callable (a function or method),
    an awaitable (an object that can be awaited, such as a coroutine), or any other object
    :return: The value returned by the `await_me_maybe` function depends on the type of the input
    `value`.

     https://simonwillison.net/2020/Sep/2/await-me-maybe/
    """
    if callable(value):
        value = value()
    if asyncio.iscoroutine(value):
        value = await value
    return value
