import logging

import streamlit as st
from mangosteen.settings import DEFAULT_STATE, DEFAULT_SHARED_STATE, LANGUAGES
from mangosteen.data import member_name_to_id_map, member_id_to_name_map

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


##############################################################################
## Utility functions
##############################################################################


def session_state_debug(debug_vars):
    return {key: val for key, val in st.session_state.items() if key in debug_vars}


def session_state_debug_vars():
    return session_state_debug(DEFAULT_STATE.keys())


def shared_session_state_debug_variables():
    return session_state_debug(DEFAULT_SHARED_STATE.keys())


def cast_param_value(param, value):
    """Cast param value to correct type"""
    # Streamlit treats all params as lists, even if they are single values
    # So we need to convert them back to single values when appropriate
    # we use the values in DEFAULT_SHARED_STATE to determine if a param
    # should be a list or a single value
    truthy_values = {"true", "1", "t", "y", "yes", "yeah", "yup", "certainly", "uh-huh"}

    if isinstance(DEFAULT_SHARED_STATE[param], list):
        return value
    if isinstance(DEFAULT_SHARED_STATE[param], bool):
        return value[0].lower() in truthy_values

    return f"{value[0]}"


## Never touch state directly, always use these functions


def set_global_session_state(key, value):
    st.session_state[key] = value


def get_global_session_state(key, default=None):
    return st.session_state.get(key, default)


def set_shared_session_state(key, value):
    assert key in DEFAULT_SHARED_STATE
    set_global_session_state(key, value)


def get_shared_session_state(key, default=None):
    assert key in DEFAULT_SHARED_STATE
    return get_global_session_state(key, default)


##############################################################################
## Setting initial state values and storing/restoring shared state from params
##############################################################################


def state_to_params():
    """Copy session state to URL"""
    for k, v in st.session_state.items():
        if k in DEFAULT_SHARED_STATE:
            st.query_params[k] = v


def params_to_state():
    for k in st.query_params:
        if k in DEFAULT_SHARED_STATE:
            st.session_state[k] = cast_param_value(k, st.query_params.get_all(k))


def new_session():
    logger.info("New session")
    st.session_state.update(DEFAULT_STATE)
    st.session_state.update(DEFAULT_SHARED_STATE)


##############################################################################
## Dealing with language and locale
##############################################################################


def currently_selected_language_name():
    selected_locale = get_shared_session_state("locale", "en-US")
    for language_name, data in LANGUAGES.items():
        if data["locale"] == selected_locale:
            return language_name
    return LANGUAGES[0]


def currently_selected_language_locale():
    return LANGUAGES[currently_selected_language_name()]["locale"]


##############################################################################
## Dealing with members
##############################################################################


def member_names():
    return list(member_name_to_id_map().keys())


def currently_selected_member_ids():
    id_strs = get_shared_session_state("selected-member-ids", [])
    return [int(id) for id in id_strs]


def member_id_to_name(id):
    return member_id_to_name_map()[id]


def member_name_to_id(name):
    return member_name_to_id_map()[name]


def currently_selected_member_names():
    return [member_id_to_name(id) for id in currently_selected_member_ids()]


##############################################################################
## Dealing with periods
##############################################################################


def currently_selected_period_id():
    return "current"


##############################################################################
## Dealing with content types
##############################################################################


def content_type_names():
    return list(st.session_state.content_type_name_to_id_map.keys())


def content_type_id_to_name(id):
    return st.session_state.content_type_id_to_name_map[id]


def content_type_name_to_id(name):
    return st.session_state.content_type_name_to_id_map[name]


def currently_selected_type_id():
    return ["journal-article"]


def showing_title_detail():
    return False


def showing_example_links():
    return False


def foo():
    pass
