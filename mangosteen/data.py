import logging
from collections import OrderedDict
import json
import pandas as pd
import streamlit as st

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def clean_names(s: pd.Series):
    return s.str.replace("&quot;", "")


def show_k_v_types(d):
    k, v = list(d.items())[0]
    logger.info(f"))))))) {type(k)}: {type(v)}")


@st.cache_data()
def member_name_to_id_map() -> dict:
    with open("data/member_name_to_id_map.json", "r") as f:
        return {k: int(v) for k, v in json.load(f).items()}


@st.cache_data()
def member_id_to_name_map() -> dict:
    with open("data/member_id_to_name_map.json", "r") as f:
        return {int(k): v for k, v in json.load(f).items()}


# def create_member_maps(df):
#     pass


# def create_title_maps(df):
#     pass


@st.cache_data()
def load_member_data():
    with st.spinner("Loading member data..."):
        return pd.read_parquet("data/members.parquet")


@st.cache_data()
def journal_names():
    return sorted(journal_name_to_id_map().keys())


@st.cache_data()
def journal_id_to_name_map():
    with open("data/journal_id_to_name_map.json", "r") as f:
        return dict(json.load(f).items())


@st.cache_data()
def journal_name_to_id_map():
    with open("data/journal_name_to_id_map.json", "r") as f:
        return dict(json.load(f).items())


@st.cache_data()
def load_title_data():
    with st.spinner("Loading title data..."):
        return pd.read_parquet("data/journals.parquet")


def create_type_maps(df):
    logger.debug("Creating type maps")
    logger.info(df.columns)
    df = df.sort_values(by=["label"])
    st.session_state.content_type_name_to_id_map = OrderedDict(
        zip(df["label"], df["id"])
    )
    st.session_state.content_type_id_to_name_map = dict(zip(df["id"], df["label"]))


def load_type_data():
    with st.spinner("Loading type data..."):
        return pd.read_parquet("data/types.parquet")


def load_data():
    load_member_data()
    load_title_data()
    create_type_maps(load_type_data())
