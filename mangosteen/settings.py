import streamlit as st

CODE_NAME = "mangosteen"
SERVICE_NAME = "Mangosteen"
PLUGINS_ENABLED_DIR = "plugins_enabled"

LANGUAGES = {
    "English": {"locale": "en-US", "flag": "🇺🇸"},
    "French": {"locale": "fr", "flag": "🇫🇷"},
    "German": {"locale": "de", "flag": "🇩🇪"},
    "Indonesian": {"locale": "id", "flag": "🇮🇩"},
}

PERIODS = ["all", "current", "backfile"]


DEFAULT_STATE = {
    "selected-member-names": [],
    "language": "English",
    "inited": True,
}


DEFAULT_SHARED_STATE = {
    "locale": "en-US",
    "selected-member-ids": [],
    "selected-period": "current",
    "selected-content-type": "journal-article",
    "showing-title-detail": False,
    "showing-example-links": False,
    "selected-title": None,
    "use-tabs": False,
}

SUPPORTS_TITLE_DETAIL = ["journal-article", "book"]
