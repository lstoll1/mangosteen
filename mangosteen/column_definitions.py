import logging
import streamlit as st
from mangosteen.l10n import _
from pandas import DataFrame

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


column_formats = [
    # "primary-name",
    # "id",
    # "account-type",
    # "non-profit",
    # "date-joined",
    # "annual-fee",
    # "active",
    # "board-status",
    # "current-dois",
    # "backfile-dois",
    # "total-dois",
    # "affiliations-current",
    # "similarity-checking-current",
    # "descriptions-current",
    # "ror-ids-current",
    # "funders-backfile",
    # "licenses-backfile",
    # "funders-current",
    # "affiliations-backfile",
    # "resource-links-backfile",
    "orcids-backfile",
    "update-policies-current",
    "ror-ids-backfile",
    "orcids-current",
    "similarity-checking-backfile",
    "references-backfile",
    "descriptions-backfile",
    "award-numbers-backfile",
    "update-policies-backfile",
    "licenses-current",
    "award-numbers-current",
    "abstracts-backfile",
    "resource-links-current",
    "abstracts-current",
    "references-current",
    "overall-coverage",
    "overall-impact",
    "earliest-publication-year",
    "latest-publication-year",
    "2010-total",
    "2011-total",
    "2012-total",
    "2013-total",
    "2014-total",
    "2015-total",
    "2016-total",
    "2017-total",
    "2018-total",
    "2019-total",
    "2020-total",
    "2021-total",
    "avg-pct-change",
    "display-name",
]


def coverage_columns(df: DataFrame):
    coverage_prefixes = ["all-", "current-", "backfile-"]
    coverage_exceptions = ["last-status-check-time"]
    return [
        column
        for column in df.columns
        if (
            column.startswith(tuple(coverage_prefixes))
            and not column.endswith(tuple(coverage_exceptions))
        )
    ]


def static_config() -> dict:
    """In the Streamlit Dataframe column_config format"""
    return {
        "primary-name": st.column_config.TextColumn(
            label=_("primary-name-label"),
            help=_("primary-name-help"),
        ),
        "primary-resource-logo": st.column_config.ImageColumn(
            label=_("primary-resource-logo-label"),
            help=_("primary-resource-logo-help"),
            width="small",
        ),
        "id": st.column_config.NumberColumn(
            label=_("id-label"), help=_("id-help"), width="small"
        ),
        "account-type": st.column_config.TextColumn(
            label=_("account-type-label"),
            help=_("account-type-help"),
        ),
        "non-profit": st.column_config.TextColumn(
            label=_("non-profit-label"),
            help=_("non-profit-help"),
        ),
        "date-joined": st.column_config.DateColumn(
            label=_("date-joined-label"),
            help=_("date-joined-help"),
            format="YYYY-MM-DD",
        ),
        "annual-fee": st.column_config.NumberColumn(
            label=_("annual-fee-label"),
            help=_("annual-fee-help"),
            format="{0:,} USD",
        ),
        "active": st.column_config.TextColumn(
            label=_("active-label"),
            help=_("active-help"),
        ),
        "board-status": st.column_config.TextColumn(
            label=_("board-status-label"),
            help=_("board-status-help"),
        ),
        "current-dois": st.column_config.NumberColumn(
            label=_("current-dois-label"),
            help=_("current-dois-help"),
            format="{0:,}",
        ),
        "backfile-dois": st.column_config.NumberColumn(
            label=_("backfile-dois-label"),
            help=_("backfile-dois-help"),
            format="{0:,}",
        ),
        "total-dois": st.column_config.NumberColumn(
            label=_("total-dois-label"),
            help=_("total-dois-help"),
            format="{0:,}",
        ),
        "primary-resource-domain": st.column_config.TextColumn(
            label=_("primary-resource-domain-label"),
            help=_("primary-resource-domain-help"),
        ),
        "affiliations-current": st.column_config.ProgressColumn(
            label=_("affiliations-current-label"),
            help=_("affiliations-current-help"),
            # format="%d",
            min_value=0,
            max_value=1,
        ),
        "similarity-checking-current": st.column_config.ProgressColumn(
            label=_("similarity-checking-current-label"),
            help=_("similarity-checking-current-help"),
            min_value=0,
            max_value=1,
        ),
        "descriptions-current": st.column_config.ProgressColumn(
            label=_("descriptions-current-label"),
            help=_("descriptions-current-help"),
            min_value=0,
            max_value=1,
        ),
        "ror-ids-current": st.column_config.ProgressColumn(
            label=_("ror-ids-current-label"),
            help=_("ror-ids-current-help"),
            min_value=0,
            max_value=1,
        ),
        "funders-backfile": st.column_config.ProgressColumn(
            label=_("funders-backfile-label"),
            help=_("funders-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "licenses-backfile": st.column_config.ProgressColumn(
            label=_("licenses-backfile-label"),
            help=_("licenses-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "funders-current": st.column_config.ProgressColumn(
            label=_("funders-current-label"),
            help=_("funders-current-help"),
            min_value=0,
            max_value=1,
        ),
        "affiliations-backfile": st.column_config.ProgressColumn(
            label=_("affiliations-backfile-label"),
            help=_("affiliations-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "resource-links-backfile": st.column_config.ProgressColumn(
            label=_("resource-links-backfile-label"),
            help=_("resource-links-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "orcids-backfile": st.column_config.ProgressColumn(
            label=_("orcids-backfile-label"),
            help=_("orcids-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "update-policies-current": st.column_config.ProgressColumn(
            label=_("update-policies-current-label"),
            help=_("update-policies-current-help"),
            min_value=0,
            max_value=1,
        ),
        "ror-ids-backfile": st.column_config.ProgressColumn(
            label=_("ror-ids-backfile-label"),
            help=_("ror-ids-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "orcids-current": st.column_config.ProgressColumn(
            label=_("orcids-current-label"),
            help=_("orcids-current-help"),
            min_value=0,
            max_value=1,
        ),
        "similarity-checking-backfile": st.column_config.ProgressColumn(
            label=_("similarity-checking-backfile-label"),
            help=_("similarity-checking-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "references-backfile": st.column_config.ProgressColumn(
            label=_("references-backfile-label"),
            help=_("references-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "descriptions-backfile": st.column_config.ProgressColumn(
            label=_("descriptions-backfile-label"),
            help=_("descriptions-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "award-numbers-backfile": st.column_config.ProgressColumn(
            label=_("award-numbers-backfile-label"),
            help=_("award-numbers-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "update-policies-backfile": st.column_config.ProgressColumn(
            label=_("update-policies-backfile-label"),
            help=_("update-policies-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "licenses-current": st.column_config.ProgressColumn(
            label=_("licenses-current-label"),
            help=_("licenses-current-help"),
            min_value=0,
            max_value=1,
        ),
        "award-numbers-current": st.column_config.ProgressColumn(
            label=_("award-numbers-current-label"),
            help=_("award-numbers-current-help"),
            min_value=0,
            max_value=1,
        ),
        "abstracts-backfile": st.column_config.ProgressColumn(
            label=_("abstracts-backfile-label"),
            help=_("abstracts-backfile-help"),
            min_value=0,
            max_value=1,
        ),
        "resource-links-current": st.column_config.ProgressColumn(
            label=_("resource-links-current-label"),
            help=_("resource-links-current-help"),
            min_value=0,
            max_value=1,
        ),
        "abstracts-current": st.column_config.ProgressColumn(
            label=_("abstracts-current-label"),
            help=_("abstracts-current-help"),
            min_value=0,
            max_value=1,
        ),
        "references-current": st.column_config.ProgressColumn(
            label=_("references-current-label"),
            help=_("references-current-help"),
            min_value=0,
            max_value=1,
        ),
        "overall-coverage": st.column_config.ProgressColumn(
            label=_("overall-coverage-label"),
            help=_("overall-coverage-help"),
            min_value=0,
            max_value=1,
        ),
        "overall-impact": st.column_config.ProgressColumn(
            label=_("overall-impact-label"),
            help=_("overall-impact-help"),
            min_value=0,
            max_value=1,
        ),
        "earliest-publication-year": st.column_config.NumberColumn(
            label=_("earliest-publication-year-label"),
            help=_("earliest-publication-year-help"),
            # format="%d",
        ),
        "latest-publication-year": st.column_config.NumberColumn(
            label=_("latest-publication-year-label"),
            help=_("latest-publication-year-help"),
            # format="%d",
        ),
        "breakdowns-dois-by-issued-year": st.column_config.BarChartColumn(
            label=_("breakdowns-dois-by-issued-year-label"),
            help=_("breakdowns-dois-by-issued-year-help"),
        ),
    }


def coverage_spec(column_name):
    label_key = f"{column_name}-label"

    label = _(label_key)
    if label == label_key:
        # logger.warning(f"Missing translation for {label_key}")
        label = column_name

    return st.column_config.ProgressColumn(
        label=label,
        help=_(f"{column_name}-help"),
        # format="%f",
        format="{:10.2f}",
        min_value=0,
        max_value=1,
    )


def dynamic_config(df) -> dict:
    return (
        {
            column_name: coverage_spec(column_name)
            for column_name in coverage_columns(df)
        }
        if df is not None
        else {}
    )


def get_column_config(df=None):
    # Note that static_config values will override dynamic_config values
    return dynamic_config(df) | static_config()
