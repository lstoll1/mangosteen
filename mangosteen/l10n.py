import logging

import streamlit as st
from fluent.runtime import FluentLocalization, FluentResourceLoader

from mangosteen.settings import LANGUAGES
from mangosteen.sessions import get_shared_session_state

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

loader = FluentResourceLoader("l10n/{locale}")
locales = [lang["locale"] for lang in LANGUAGES.values()]
l10n = FluentLocalization(locales, ["main.ftl"], loader)


def _(k):
    locale = get_shared_session_state("locale")
    logger.debug(f"** {k} ---> locale is {locale}")
    lang_order = [locale, "en-US"]
    logger.debug(f"lang order is {k} to {lang_order}")
    l10n = FluentLocalization(lang_order, ["main.ftl"], loader)
    return l10n.format_value(k)
