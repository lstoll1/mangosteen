import logging

import streamlit as st
from streamlit_extras.no_default_selectbox import selectbox
from PIL import Image

from mangosteen.l10n import _
from mangosteen.plugins import enabled_plugins_info, reload_plugins
from mangosteen.sessions import (
    content_type_id_to_name,
    content_type_name_to_id,
    content_type_names,
    currently_selected_language_name,
    currently_selected_member_names,
    get_global_session_state,
    get_shared_session_state,
    member_name_to_id,
    member_names,
    session_state_debug_vars,
    shared_session_state_debug_variables,
    set_global_session_state,
    set_shared_session_state,
    currently_selected_member_ids,
)
from mangosteen.data import load_member_data, load_title_data
from mangosteen.settings import LANGUAGES, SERVICE_NAME, SUPPORTS_TITLE_DETAIL
from mangosteen.colors import CR_PRIMARY_DK_GREY
from mangosteen.metrics import Metrics


METRICS = Metrics()


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def language_to_locale(language):
    return LANGUAGES[language]["locale"]


def language_changed():
    lang = get_global_session_state("language")
    set_global_session_state("locale", language_to_locale(lang))
    set_shared_session_state("locale", language_to_locale(lang))
    METRICS.language_selection_counter.labels(lang).inc()


def members_changed():
    # logger.debug("select_members")
    selected_member_names = get_global_session_state("selected-member-names")
    member_ids = [member_name_to_id(name) for name in selected_member_names]
    METRICS.number_of_members_selected_counter.labels(len(member_ids)).inc()
    # METRICS.member_selection_counter.labels(*member_ids).inc()
    [
        METRICS.member_selection_counter.labels(name).inc()
        for name in selected_member_names
    ]
    set_shared_session_state("selected-member-ids", member_ids)

    # set_global_session_state(
    #     "selected-member-names", get_global_session_state("selected-member-names")
    # )
    if len(member_ids) > 1 or len(member_ids) == 0:
        set_shared_session_state("selected-title", None)

    logger.debug(f"Selected: {get_shared_session_state('selected-member-ids')}")


def content_type_changed(plugin_index: int):
    session_state_key = f"selected_content_type_{plugin_index}"
    content_name = get_global_session_state(session_state_key)
    set_global_session_state("selected-content-type-name", content_name)
    content_type_id = content_type_name_to_id(content_name)
    set_shared_session_state("selected-content-type", content_type_id)


def period_changed(plugin_index: int):
    logger.info(f"period_changed for plugin: {plugin_index}")
    session_state_key = f"selected_period_{plugin_index}"
    period_name = get_global_session_state(session_state_key)
    set_global_session_state("selected-period", period_name)
    set_shared_session_state("selected-period", period_name)
    logger.info(f">>>> period_name: {period_name}")


def display_period_filter(index: int):
    period_name = get_shared_session_state("selected-period", "all")
    set_global_session_state(f"selected_period_{index}", period_name)
    st.selectbox(
        _("period-filter-label"),
        ["all", "current", "backfile"],
        index=0,
        help=_("period-filter-help"),
        key=f"selected_period_{index}",
        on_change=period_changed,
        kwargs={"plugin_index": index},
        format_func=lambda x: _(f"period-{x}-label"),
    )


def display_content_type_filter(index: int):
    # TODO update to use format and _ function
    content_type_id = get_shared_session_state("selected-content-type", None)
    content_type_name = content_type_id_to_name(content_type_id)
    set_global_session_state(f"selected_content_type_{index}", content_type_name)
    st.selectbox(
        _("content-type-filter-label"),
        content_type_names(),
        index=0,
        help=_("content-type-filter-help"),
        key=f"selected_content_type_{index}",
        on_change=content_type_changed,
        kwargs={"plugin_index": index},
    )


def title_filter_enabled():
    return (
        len(get_shared_session_state("selected-member-ids")) == 1
        and get_shared_session_state("selected-content-type") in SUPPORTS_TITLE_DETAIL
    )


def selected_title_changed():
    METRICS.journal_title_selected_counter.inc()


def display_title_filter(index: int):
    selected_member_id = currently_selected_member_ids()[0]
    # HERE
    df = load_title_data()
    titles = sorted(df[df["member-id"] == selected_member_id]["title"].to_list())

    selectbox(
        _("title-filter-label"),
        titles,
        key="selected-title",
        help=_("title-filter-help"),
        no_selection_label="<All>",
        on_change=selected_title_changed,
    )


def display_content_filter(index: int):
    row1 = st.columns(4)
    with row1[0]:
        display_period_filter(index=index)
    with row1[1]:
        display_content_type_filter(index=index)

    row2 = st.columns(2)
    with row2[0]:
        if title_filter_enabled():
            display_title_filter(index=index)
    st.divider()


def display_enabled_plugins():
    plugin_info = enabled_plugins_info()
    with st.expander("Enabled Plugins"):
        st.json(plugin_info)


def display_shared_state():
    with st.expander("Shared Session State"):
        st.markdown("Shared session state is shareable via url")
        st.json(shared_session_state_debug_variables())


def display_session_state():
    # debug_vars = ["selected-member-names", "language"]
    with st.expander("Unshared Session State"):
        st.markdown("Unshared session state is not shareable via url")
        st.json(session_state_debug_vars())


def display_reload_plugins():
    st.button("Reload Plugins", on_click=reload_plugins)


def display_language_selector_icon():
    image = Image.open("static/lang.png")
    st.image(image, width=25)


def display_language_selector():
    display_language_selector_icon()
    set_global_session_state("language", currently_selected_language_name())

    st.selectbox(
        "Language",
        list(LANGUAGES.keys()),
        key="language",
        on_change=language_changed,
        label_visibility="collapsed",
    )
    st.divider()


def display_member_selector():
    set_global_session_state("selected-member-names", currently_selected_member_names())
    st.header(_("member-selector-heading"))
    st.multiselect(
        _("member-selector-label"),
        member_names(),
        help=_("member-selector-help"),
        placeholder=_("member-selector-placeholder"),
        on_change=members_changed,
        key="selected-member-names",
    )


def display_git_commit():
    # if COMMIT file exists, read its contents into commit
    commit = "development"
    try:
        with open("COMMIT", "r") as f:
            commit = f.read().strip()
    except FileNotFoundError:
        commit = "dev"

    st.text(f"Git: {commit}")


def display_sidebar():
    with st.sidebar:
        st.markdown(f"<!-- {SERVICE_NAME}-sidebar-->", unsafe_allow_html=True)
        st.image(
            "https://assets.crossref.org/logo/labs/labs-logo-ribbon.svg", width=200
        )
        st.header(SERVICE_NAME)
        display_language_selector()
        display_member_selector()
        st.divider()
        st.subheader(":mag: debug info")
        display_enabled_plugins()
        display_shared_state()
        display_session_state()
        display_reload_plugins()
        display_git_commit()


def display_plugin_metadata(plugin):
    st.markdown(
        f'<div class="cr-plugin-header">{plugin.title()}</div>',
        unsafe_allow_html=True,
    )
    st.markdown(
        f'<div class="cr-plugin-description">{plugin.description()}</div>',
        unsafe_allow_html=True,
    )
    st.markdown(
        f'<div class="cr-plugin-version">V: {plugin.version()}</div>',
        unsafe_allow_html=True,
    )


def desired_container_type(titles):
    use_tabs = get_shared_session_state("use-tabs")
    return (
        list(st.tabs(titles))
        if use_tabs
        else [st.container(border=True) for _ in titles]
    )


def display_results():
    plugins_info = enabled_plugins_info()
    titles = [item["plugin"].title() for item in plugins_info]

    containers = desired_container_type(titles)

    for index, container in enumerate(containers):
        plugin = plugins_info[index]["plugin"]
        with container:
            display_plugin_metadata(plugin)
            if "show_content_type_filter" in plugins_info[index]["hooks"]:
                logger.info("show content type filter")
                display_content_filter(index=index)
            else:
                logger.info("hide content type filter")
            plugin.display_data(
                data=load_member_data(),
            )


def display_splash():
    st.markdown(f"<!-- {SERVICE_NAME}-splash-->", unsafe_allow_html=True)
    st.header(_("splash-screen"))
