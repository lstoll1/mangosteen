import typer

from mangosteen.plugins import pm  # , plugins_list


def main(verbose: bool = typer.Option(False, "--verbose", "-v")):
    if verbose:
        typer.echo(plugins_list())

    titles = pm.hook.title()
    typer.echo(titles)

    load_results = pm.hook.load_data(source="foobar")
    typer.echo(load_results)

    display_results = pm.hook.display_data(data="wangbang")
    typer.echo(display_results)


def cli():
    typer.run(main)


if __name__ == "__main__":
    typer.run(main)
