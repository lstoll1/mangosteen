import json
import logging

# import streamlit as st
from pandas import DataFrame
from streamlit_extras.grid import grid

from mangosteen.colors import CR_PRIMARY_GREEN
from mangosteen.column_definitions import get_column_config

COVERAGE_COLUMNS = [
    "abstracts-backfile",
    "abstracts-current",
    "affiliations-backfile",
    "affiliations-current",
    "award-numbers-backfile",
    "award-numbers-current",
    "descriptions-backfile",
    "descriptions-current",
    "funders-backfile",
    "funders-current",
    "licenses-backfile",
    "licenses-current",
    "orcids-backfile",
    "orcids-current",
    "overall-coverage",
    "overall-impact",
    "references-backfile",
    "references-current",
    "resource-links-backfile",
    "resource-links-current",
    "ror-ids-backfile",
    "ror-ids-current",
    "similarity-checking-backfile",
    "similarity-checking-current",
    "update-policies-backfile",
    "update-policies-current",
]


LABEL_WIDTH = 1
COLUMN_WIDTH = 2


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_grid_spec(
    number_of_columns,
    number_of_rows,
    label_width=LABEL_WIDTH,
    column_width=COLUMN_WIDTH,
):
    col_spec = [label_width] + [column_width] * number_of_rows
    return [col_spec] * number_of_columns


def display_text_column(context, dp, dp_config):
    context.markdown(f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True)


def display_number_column(context, dp, dp_config):
    if fmt := dp_config["type_config"]["format"]:
        dp = fmt.format(int(dp))
    context.markdown(f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True)


def display_date_column(context, dp, dp_config):
    if fmt := dp_config["type_config"]["format"]:
        dp = fmt.format(int(dp))
    context.markdown(f'<p class="cr-gridify-cell">{dp}</p>', unsafe_allow_html=True)


def display_progress_column(context, dp, dp_config):
    fmt = dp_config.get("type_config", {}).get("format", None)
    dp_text = fmt.format(dp * 100) if format else dp
    context.progress(dp, text=f"{dp_text}%")


def display_bar_chart_column(context, dp, dp_config):
    data = json.loads(dp)
    context.bar_chart(data, x="years", y="counts", color=CR_PRIMARY_GREEN)


def display_image_column(context, dp, dp_config):
    context.image(dp)


def column_type(dp_config):
    return dp_config["type_config"]["type"]


def column_format(dp_config):
    return dp_config.get("type_config", {}).get("format", None)


def help_text(dp_config):
    return dp_config.get("help", None)


COLUMN_TYPE_TO_DISPLAY_FUNCTION = {
    "text": display_text_column,
    "number": display_number_column,
    "progress": display_progress_column,
    "bar_chart": display_bar_chart_column,
    "image": display_image_column,
    "date": display_text_column,
}


def lookup_display_function(dp_config):
    return COLUMN_TYPE_TO_DISPLAY_FUNCTION.get(column_type(dp_config))


def display_data_point(context, dp, dp_config):
    if display_function := lookup_display_function(dp_config):
        display_function(context, dp, dp_config)
    else:
        logger.info(f"!!!!!! no display function for {dp_config}")
        context.write(dp)


def make_grid(grid_spec):
    return grid(
        *grid_spec,
        vertical_align="center",
        gap="medium",
    )


def get_label_keys(df, pivot_column):
    # labels are all the column names except the pivot column name
    return [col for col in df.columns.tolist() if col != pivot_column]


def create_row_labels(column_config, label_keys):
    return [
        column_config.get(label_key, {"label": label_key})["label"]
        for label_key in label_keys
    ]


def create_row_help(column_config, label_keys):
    return [
        column_config.get(label_key, {"help": None})["help"] for label_key in label_keys
    ]


def extract_data(df, label_keys):
    # Data is a list of lists
    # data is the values of df except the value of the pivot column
    return [df[label_key].tolist() for label_key in label_keys]


def new_column_names(df, pivot_column):
    # the values of the pivot column will become the new column names
    return df[pivot_column].tolist()


def display_column_names(my_grid, df, pivot_column):
    my_grid.empty()
    for label in new_column_names(df, pivot_column):
        my_grid.markdown(
            f'<div class="cr-gridify-column-label">{label}</div>',
            unsafe_allow_html=True,
        )


def gridify(df: DataFrame, pivot_column, grid_spec):
    # create a grid with the number of rows and columns we need
    data_grid = make_grid(grid_spec)

    # Get formatting information for the columns
    column_config = get_column_config(df)

    # in the dataframe they are the column names, in the grid, they will be row label names
    row_keys = get_label_keys(df, pivot_column)
    row_labels = create_row_labels(column_config, row_keys)
    row_help = create_row_help(column_config, row_keys)

    # The columns names will be the values of the pivot_column in the Dataframe
    display_column_names(data_grid, df, pivot_column)

    data = extract_data(df, row_keys)

    for row_index, data_row in enumerate(data):
        data_grid.markdown(
            f'<span class="cr-gridify-row-label">{row_labels[row_index]}</span>',
            unsafe_allow_html=True,
            help=row_help[row_index],
        )

        for data_point in data_row:
            data_point_config = column_config.get(row_keys[row_index], None)
            display_data_point(data_grid, data_point, dp_config=data_point_config)
