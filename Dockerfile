# syntax=docker/dockerfile:1
FROM python:3.11-slim
ARG COMMIT
ENV COMMIT=$COMMIT
RUN apt-get update; apt-get install -y curl
# unzip gcc python3-dev
WORKDIR /code
# set virtual env
ENV VIRTUAL_ENV=/opt/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
# RUN od -An -N1 -i /dev/random > random_num


COPY .streamlit/ .streamlit/
COPY app.py .
COPY data/ data/
COPY l10n/ l10n/
COPY mangosteen/ mangosteen/
COPY plugins_available/ plugins_available/
COPY plugins_enabled/ plugins_enabled/
COPY prepare-data.py .
COPY pyproject.toml .
COPY refresh-journal-data.sh .
COPY refresh-member-data.sh .
COPY static/ static/
RUN pip install --upgrade pip
RUN pip install .


EXPOSE 5000

ARG CACHE_DATE=2023-07-03

# As of 2023-01-16 the following is not working because the Labs REST API is not working
# RUN apt-get install curl
# RUN ./refresh-member-data.sh
# RUN python3 prepare-data.py --file members.json 
# RUN rm members.json
# RUN ./refresh-journal-data.sh
# RUN python3 prepare-data.py --file journals.json 
# RUN rm journals.json

RUN echo $COMMIT > COMMIT   

# NB turn off file watcher for streamlit because iwatch doesn't like working with so many
# files in the data directory in the container
CMD ["streamlit", "run","app.py"]
