import logging

# from collections import OrderedDict
# import pandas as pd
import streamlit as st
from mangosteen.plugins import reload_plugins
from mangosteen.sessions import (
    new_session,
    params_to_state,
    state_to_params,
)
from mangosteen.data import load_data
from mangosteen.ui import display_sidebar, display_splash, display_results

from mangosteen.settings import CODE_NAME
from mangosteen.metrics import Metrics

METRICS = Metrics()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


logger.info("Loading streamlit")


def is_new_session() -> bool:
    return "existing_session" not in st.session_state


def members_are_selected() -> bool:
    # return st.session_state.shared_state["selected-member-ids"] != []
    return st.session_state["selected-member-ids"] != []


def insert_css():
    with open("static/app.css") as css:
        st.markdown(f"<style>{css.read()}</style>", unsafe_allow_html=True)


st.set_page_config(page_title=CODE_NAME, page_icon=None, layout="wide")
insert_css()

## Main

# This is a singleton. It should be safe to reload.
# METRICS = Metrics()

if "inited" not in st.session_state:
    # This is the first time the app is run
    # set everything to the default state
    # and then override defaults with any
    # **allowed* params in the URL
    logger.info("Initializing session state")
    new_session()
    params_to_state()
    load_data()
    reload_plugins()
    METRICS.new_session_counter.inc()
else:
    METRICS.existing_session_counter.inc()

display_sidebar()

# If we are inited *or* this is a continued session,
# then we want to copy the **allowed** params to the URL
state_to_params()

# And then we go about our main business
if members_are_selected():
    display_results()
else:
    display_splash()
