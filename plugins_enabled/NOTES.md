# Notes


## Session handling

The standard way to handle sessions in streamlit is to use `st.session_state.foo`.

The problem is that I'd rather not give plugins complete access to all of streamlit's state as this will make it very hard to make changes to the framework and know that they will not effect plugins.

I can't *prevent* users from accessing streamlit's session_state directly, but I can at least give plugin users a namespaced set of session data that *is* safe to use.

In mangosteen's case I am exposing session variables to plugins through tha dictionary that I keep called `shared_state`.
