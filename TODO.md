# TODO


## For basic plugin demo

- plugin tests
- plugin support files

## For Reports

- Global Selector
- Parameter saving/restore
- Metrics
- Localization
- Auth guts

## Notes

### Governance

It would be useful if we could include member governance information in the API and reports.

For example, that a member was on the board 2001,2002,2003,2004
Of on the nominating committee 2001,2002
